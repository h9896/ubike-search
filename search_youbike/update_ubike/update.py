"""This file is for updating db."""
import time
import requests
import logging
from django.db import models
from apscheduler.schedulers.background import BackgroundScheduler
from update_ubike.models import Ubike, UbikeDistance, get_distance

YOUBIKE_URL = "https://tcgbusfs.blob.core.windows.net/blobyoubike/YouBikeTP.json"
AVG_LAT = {'lat__avg':0}
AVG_LNG = {'lng__avg':0}
FIRST = True

necessary_val = ['sno', 'lng', 'lat', 'sbi', 'sna', 'snaen', 'bemp', 'act']
#sno: station id
#lng: longitude
#lat: latitude
#sbi: numbers of ubike can borrow
#sna: station name
#snaen: station name in English
#bemp: number of ubike can retrun
#act: the station state
def is_station_id_exist(key) -> bool:
    return Ubike.objects.filter(sno=key).exists()
def create_db_data_by_station_id(new_value):
    new_value = {key: new_value[key] for key in necessary_val}
    Ubike.objects.create(**new_value)
def update_db_data(new_value):
    db_data = Ubike.objects.filter(sno=new_value['sno'])
    new_value = {key: new_value[key] for key in necessary_val}
    del(new_value['sno'])
    db_data.update(**new_value)
def update_db_job():
    """This function is used to update ubike data."""
    logging.info("Start update DB at {}".format(time.strftime("%Y-%m-%d %H:%M:%S",
                                        time.localtime(time.time()))))
    response = requests.get(YOUBIKE_URL)
    data = response.json()
    if data['retCode'] == 1:
        for _, value in data['retVal'].items():
            # station id as key
            if is_station_id_exist(value['sno']):
                update_db_data(value)
            else:
                create_db_data_by_station_id(value)
                if not FIRST:
                    new_dis = get_distance(value['lat'], value['lng'], \
                        AVG_LAT['lat__avg'], AVG_LNG['lng__avg'])
                    UbikeDistance.objects.create(sno=value['sno'],distance=new_dis)
        logging.info("Finish update DB at {}".format(time.strftime('%Y-%m-%d %H:%M:%S',
                                            time.localtime(time.time()))))
        if FIRST:
            change_avg_lat()
            change_avg_lng()
            for ubike in Ubike.objects.all():
                dis = get_distance(ubike.lat, ubike.lng, AVG_LAT['lat__avg'], AVG_LNG['lng__avg'])
                if UbikeDistance.objects.filter(sno=ubike.sno).exists():
                    db_distance = UbikeDistance.objects.filter(sno=ubike.sno)
                    db_distance.update(distance=dis)
                else:
                    UbikeDistance.objects.create(sno=ubike.sno,distance=dis)
            change_first_flag()
            logging.info("Finish update UbikeDistance at {}".format(time.strftime('%Y-%m-%d %H:%M:%S',
                                            time.localtime(time.time()))))

def change_avg_lat():
    '''change global value AVG_LAT'''
    global AVG_LAT
    AVG_LAT = Ubike.objects.aggregate(models.Avg('lat'))
def change_avg_lng():
    '''change global value AVG_LNG'''
    global AVG_LNG
    AVG_LNG = Ubike.objects.aggregate(models.Avg('lng'))
def change_first_flag():
    '''change global value FIRST'''
    global FIRST
    if FIRST:
        FIRST = False
    else:
        FIRST = True

def start():
    '''start scheduler'''
    scheduler = BackgroundScheduler()
    scheduler.add_job(update_db_job, 'interval', \
        seconds=90, id='update_db_job', replace_existing=True)
    if not scheduler.running:
        scheduler.start()

